clear
//CONSTANTS

TileCellSize = 16
NOISE_OFFSET = 8

//IMPORTS


import "GameData"
import "GamePhysics"
import "GameMath"
import "DetectableNoise"
import "LevelLoader"
import "LevelsOperation1"
import "LevelRotation"

import "GameActor"
import "Bullet"
import "GameHud"
import "Views"
import "soundbank"

globals.lowFps = 0

//Functions

start_sequence = function()
    clear
    text.delimiter = ""
    print("Starting Game")

    for i in range (0,2)
        print(".")
        wait(0.2)
    end for
    clear
end function


initialize_player_character_animations = function()
    GameData.player.addAnimation("run")
    GameData.player.animations["run"].addImageDirectory("/usr/game/sprites/player_character_ninja_run")

    GameData.player.animations["run"].framesPerSecond = 16

    GameData.player.addAnimation("default")
    GameData.player.animations["default"].addImage("/usr/game/sprites/player_character_ninja.png")

    GameData.player.updateAnimationAccordingToMovementVector = function()
        if self.movement.x == 0 then
            self.switchAnimation("default")
        else if self.movement.x > 0 then
            self.switchAnimation("run")
            self.scale = 2
            if self.isOnFloor or self.isOnCeiling then
                self.playAnimation()
                if (time - self.lastNoise > self.noNoiseTime) then
                    self.lastNoise = time
                    GameData.detectableNoises = GameData.detectableNoises + [DetectableNoise.newDetectableNoise(GameMath.Vector.newVec(GameData.player.x + NOISE_OFFSET,GameData.player.y),100,0.5)]
                    soundbank.noise1.play
                end if
            else 
                self.stopAnimation()
            end if
        else
            self.switchAnimation("run")
            self.scale = [-2, 2]
            if self.isOnFloor or self.isOnCeiling then
                self.playAnimation()
                if (time - self.lastNoise > self.noNoiseTime) then
                    self.lastNoise = time
                    GameData.detectableNoises = GameData.detectableNoises + [DetectableNoise.newDetectableNoise(GameMath.Vector.newVec(GameData.player.x - NOISE_OFFSET,GameData.player.y),100,0.5)]
                    soundbank.noise1.play
                end if
            else
                self.stopAnimation()
            end if
        end if
    end function

end function

initialize_player_character = function()
    initialize_player_character_animations()
    GameData.player.switchAnimation("run")
    GameData.player.playAnimation()
    GameData.player.update()

    GameData.player.scale = 2

    GameData.player.localBounds = new Bounds
    GameData.player.localBounds.width = GameData.player.image.width-2
    GameData.player.localBounds.height = GameData.player.image.height-1

end function

move_actor = function(actor, movement)
    actor.x = actor.x + movement.x
    actor.y = actor.y + movement.y
    actor.lastMovement.x = movement.x
    actor.lastMovement.y = movement.y
end function

updateNoises = function()
    display(2).clear
    noisesToDelete = []
    for noise in GameData.detectableNoises
        noise.visualize
        if not noise.alive then
            noisesToDelete = noisesToDelete + [noise]
        end if
    end for
    for noise in noisesToDelete
        GameData.detectableNoises.remove(GameData.detectableNoises.indexOf(noise))
    end for
end function

process = function(delta)
    Views.currentView.update(delta)

end function

//Program Start

load "startup.ms"


clear

display(2).mode = displayMode.pixel
display(2).clear
display(1).mode = displayMode.pixel
display(1).clear

display(0).mode = displayMode.pixel
display(0).clear
//GameData.level = LevelLoader.loadNextLevel(LevelsOperation1,-1)

initialize_player_character()

Views.GameMenuView.nextLevel=-1
Views.GameMenuView.show()

//soundbank.background.loop = 1
//soundbank.background.play

GameData.lastUpdate = time
while Views.currentView!=Views.GameMenuView or not key.pressed("escape")
    
    //wait 0.01
    delta = time-GameData.lastUpdate
    if key.pressed("p") then print delta
    if globals.lowFps then
        delta = GameMath.min([delta,0.04])
    else
        delta = GameMath.min([delta,0.01])
    end if
    GameData.lastUpdate = time
    //wait 0.1
    process(delta)
    if globals.lowFps then
        wait((1/15)- delta)
    else
        wait((1/75)- delta)
    end if
end while
key.clear
clear
//soundbank.background.stop
text.delimiter = char(13)
print ""
