
import "GameActor"
import "GameMath"
import "GamePhysics"

BULLET_SPEED = 3

Bullet = new GameActor.GameActor

Bullet.velocity = new GameMath.Vector.newVec(0,0)

Bullet.update = function()
    self.animations[self.currentAnimation].update()
    self.updateImageFromAnimation(self.currentAnimation)
    temporary_bullet = GamePhysics.tileCollisionCalculation(self)
    if temporary_bullet.isOnCeiling or temporary_bullet.isOnWall or temporary_bullet.isOnFloor then
        GameData.actors.remove(GameData.actors.indexOf(self))
        GameData.bullets.remove(GameData.bullets.indexOf(self))
    end if
    if GameData.player.worldBounds.overlaps(self.worldBounds) then
        GameData.actors.remove(GameData.actors.indexOf(self))
        GameData.bullets.remove(GameData.bullets.indexOf(self))
        GameData.player.controller.hit(GameData.getCurrentPlayerUpgrades.turretDmg)
    end if
end function

newBullet = function(position, target)
    retBullet = new Bullet
    retBullet.addAnimation("default")
    retBullet.animations["default"].addImage("/usr/game/sprites/bullet.png")
    retBullet.switchAnimation("default")
    retBullet.updateImageFromAnimation(retBullet.currentAnimation)
    retBullet.playAnimation()


    retBullet.localBounds = new Bounds
    retBullet.localBounds.width = retBullet.image.width
    retBullet.localBounds.height = retBullet.image.height

    retBullet.x = position.x
    retBullet.y = position.y
    if globals.lowFps then
        retBullet.movement = GameMath.Vector.sum(target, GameMath.Vector.newVec(-position.x, -position.y)).normalize().scalarProduct(BULLET_SPEED*GameData.getCurrentPlayerUpgrades.bulletSpeed*2.5)
    else
        retBullet.movement = GameMath.Vector.sum(target, GameMath.Vector.newVec(-position.x, -position.y)).normalize().scalarProduct(BULLET_SPEED*GameData.getCurrentPlayerUpgrades.bulletSpeed)
    end if
    
    return retBullet
end function