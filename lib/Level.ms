
//Data about Level
//Functions for Init, WinLossCheck
import "Upgrades"
import "Turret"
import "soundbank"

tileFile = "/usr/game/data/test_level_2.json"
win=0
lost=0
goal=0
levelUpgrades = {}
tempUpgrades = {}


init = function ()
    self.levelUpgrades = new Upgrades
end function

setTiles = function(display, tileArray)
    for row in range(0, tileArray.len-1)
        for col in range(0, tileArray[row].len-1)
            display.setCell(col, tileArray.len-row-1, tileArray[row][col])
        end for
    end for
end function

initTile = function(tileArray)
    display(5).mode = displayMode.tile
    tiles = display(5)
    display(5).mode = displayMode.off

    tiles.tileSet = file.loadImage("/usr/game/sprites/tileset.png")
    tiles.tileSetTileSize = 8
    tiles.cellSize = TileCellSize
    tiles.extent = [60,40]
    tiles.clear
    setTiles(tiles, tileArray)
    display(5).mode = displayMode.tile
end function

initTurrets = function(turretArray)
    for turret in turretArray
    
       GameData.actors = GameData.actors + [Turret.newTurret(GameMath.Vector.newVec(turret.x*2, display(2).height-turret.y*2-16))]
    end for
end function

initFromDictionary = function(dictionary)
    
    GameData.actors = []
    self.initTile(dictionary["tiles"])
    self.initTurrets(dictionary["turrets"])
    self.goal = GameMath.Vector.newVec(dictionary["goal"][0].x*2,display(2).height-dictionary["goal"][0].y*2)
    
    self.tempUpgrades={}+self.levelUpgrades
    
    goalActor = new GameActor.GameActor
    goalActor.scale = 2
    goalActor.x = self.goal.x
    goalActor.y = self.goal.y
    goalActor.addAnimation("default")
    goalActor.animations["default"].addImage("/usr/game/sprites/guard_character.png")
    goalActor.switchAnimation("default")
    goalActor.playAnimation()
    goalActor.update()
    GameData.actors = GameData.actors + [goalActor]

    GameData.player.x = dictionary["start"][0].x*2
    GameData.player.y = display(2).height-dictionary["start"][0].y*2
    GameData.actors = GameData.actors + [GameData.player]
    self.loaded=1
end function

updateLevelWinLossCheck = function ()
    if self.goal then
        if GameData.player.worldBounds.contains(self.goal.x,self.goal.y) then
            self.win=true
            soundbank.win.play
        end if
    end if
    if GameData.getCurrentPlayerUpgrades.playerCurrentHealth<=0 then
        self.lost=true
    end if
end function

updateLevelConditions = function ()
    self.updateLevelWinLossCheck()
    // print self.goal.x+" "+self.goal.y
    // print self.lost
end function