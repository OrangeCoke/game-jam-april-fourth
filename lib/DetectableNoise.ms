import "GameMath"

DetectableNoise = {}

DetectableNoise.position = GameMath.Vector.newVec(0,0)

DetectableNoise.timeOfInit = time
DetectableNoise.lifeTime = 1
DetectableNoise.radius = 10

DetectableNoise.alive = function()
    return self.timeOfInit + self.lifeTime >= time
end function

DetectableNoise.currentRadius = function()
    return self.radius*(time-self.timeOfInit)/self.lifeTime
end function

DetectableNoise.visualize = function()
    if self.alive then
        display(2).drawEllipse(self.position.x - self.currentRadius, self.position.y - self.currentRadius, 2*self.currentRadius, 2*self.currentRadius, "#FFFFFFFF")
    end if
end function

DetectableNoise.resetTime = function()
    self.timeOfInit = time
end function

newDetectableNoise = function(position, radius, lifeTime)
    newNoise = new DetectableNoise
    newNoise.position = position
    newNoise.radius = radius
    newNoise.lifeTime = lifeTime
    newNoise.resetTime

    return newNoise
end function

