
import "GameMath"
import "Animation"
import "DetectableNoise"
import "soundbank"

GameActor = new Sprite

GameActor.movement = new GameMath.Vector.newVec(0,0)
GameActor.lastMovement = new GameMath.Vector.newVec(0,0)

GameActor.isOnFloor =   false
GameActor.isOnCeiling = false
GameActor.isOnWall =    false


GameActor.images = {}
GameActor.animations = {}
GameActor.currentAnimation = "default"

GameActor.resetCollision = function()
    self.isOnFloor =   false
    self.isOnCeiling = false
    self.isOnWall =    false
end function
GameActor.addImage = function(name, path)
    self.images[name] = file.loadImage(path)
end function

GameActor.setImage = function(name)
    self.image = self.images[name]
end function

GameActor.addAnimation = function(name)
    self.animations[name] = new Animation
end function

GameActor.updateImageFromAnimation = function(animation)
    self.image = self.animations[animation].frame()
end function

GameActor.update = function()
    self.animations[self.currentAnimation].update()
    self.updateImageFromAnimation(self.currentAnimation)
end function

GameActor.switchAnimation = function(animation)
    if animation != self.currentAnimation then
        self.animations[self.currentAnimation].stop()
        self.currentAnimation = animation
    end if
end function

GameActor.playAnimation = function()
    self.animations[self.currentAnimation].play()
end function

GameActor.stopAnimation = function()
    self.animations[self.currentAnimation].stop()
end function

GameActor.controller = {}

GameActor.updateAnimationAccordingToMovementVector = function()
    print "NOT IMPLEMENTED"
end function

PlayerController= {}

PlayerController.last_jump = -100

PlayerController.get_movement_dir = function()
    direction = new GameMath.Vector
    if key.pressed("w") and (GameData.player.isOnFloor or (GameData.player.isOnWall and GameData.getCurrentPlayerUpgrades.wallJump)) then
        direction.y = direction.y + 1
        self.last_jump = time
        soundbank.jump.play
    end if 
    if key.pressed("w") and GameData.player.isOnCeiling and GameData.getCurrentPlayerUpgrades.ceilingClimb then
        if globals.lowFps then
            direction.y = direction.y + 0.4
        else
            direction.y = direction.y + 0.2
        end if
    end if
    if key.pressed("w") and GameData.player.isOnWall and GameData.getCurrentPlayerUpgrades.wallClimb then
        direction.y = direction.y + 0.5
    end if
    if key.pressed("s") then
        direction.y = direction.y
    end if
    if key.pressed("d") then
        direction.x = direction.x + 1
    end if
    if key.pressed("a") then
        direction.x = direction.x - 1
    end if
    return direction
end function

PlayerController.hit = function(dmg)
    soundbank.damage.play
    if GameData.level.tempUpgrades.playerCurrentHealth==-1 then
        GameData.level.tempUpgrades.playerCurrentHealth=GameData.getCurrentPlayerUpgrades.playerCurrentHealth
    end if
    GameData.level.tempUpgrades.playerCurrentHealth=GameMath.max([GameData.level.tempUpgrades.playerCurrentHealth-dmg,0])
end function