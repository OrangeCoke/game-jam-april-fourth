
//Collection of all GameData


import "Level"//Data and Functions about current level
import "GameActor"//Data about Actors
import "GameMath"
import "DetectableNoise"
import "Upgrades"
import "listUtil"
import "soundbank"

level = new Level
level.loaded=0
lastUpdate=0
inLevelRotation=false
difficulty=1
actors = []
detectableNoises = []
bullets = []

currentView = {}

player = new GameActor.GameActor
player.controller = new GameActor.PlayerController
player.noNoiseTime = 0.125
player.lastNoise = time
player.lastFloorStatus = false
player.lastisOnFloorTime = time-1
player.lastCeilingStatus = false
player.lastisOnCeilingTime = time-1
player.upgrades=new Upgrades
player.upgrades.setToDefaultUpgrades

player.update = function()
    self.animations[self.currentAnimation].update()
    self.updateImageFromAnimation(self.currentAnimation)

    
    if time-self.lastisOnCeilingTime<0.1 then self.lastCeilingStatus=true
    if self.isOnCeiling then self.lastisOnCeilingTime = time
    

    if(self.isOnCeiling and not self.lastCeilingStatus) then
        GameData.detectableNoises = GameData.detectableNoises + [DetectableNoise.newDetectableNoise(GameMath.Vector.newVec(self.x ,self.y),150,0.8)]
        soundbank.noise2.play
        self.lastNoise = time
    end if

    
    if time-self.lastisOnFloorTime<0.1 then self.lastFloorStatus=true
    if self.isOnFloor then self.lastisOnFloorTime = time

    if(self.isOnFloor and not self.lastFloorStatus) then
        GameData.detectableNoises = GameData.detectableNoises + [DetectableNoise.newDetectableNoise(GameMath.Vector.newVec(self.x ,self.y),150,0.8)]
        soundbank.noise2.play
        self.lastNoise = time
    end if

    self.lastFloorStatus = self.isOnFloor
    self.lastCeilingStatus = self.isOnCeiling
end function

actors=actors+[player]


getCurrentPlayerUpgrades = function ()
    return GameData.player.upgrades.getMergedUpgrades(GameData.level.tempUpgrades)
end function


