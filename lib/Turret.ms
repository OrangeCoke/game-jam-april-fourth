import "GameActor"
import "GameMath"
import "soundbank"

Turret = new GameActor.GameActor
Turret.addAnimation("default")
Turret.animations["default"].addImage("/usr/game/sprites/turret_obstacle.png")
Turret.switchAnimation("default")
Turret.playAnimation()
Turret.update()

Turret.scale = 2
Turret.active = false
Turret.activeSince = 0
Turret.target = new GameMath.Vector.newVec(0, 0)
Turret.timeOnTarget = 1

Turret.update = function()
    self.animations[self.currentAnimation].update()
    self.updateImageFromAnimation(self.currentAnimation)
    if not self.active then
        for noise in GameData.detectableNoises
            if noise.position.distance(new GameMath.Vector.newVec(self.x, self.y)) < noise.currentRadius then
                self.active = true
                self.target = noise.position
                self.activeSince = time
                temporary_bullet = [Bullet.newBullet(new GameMath.Vector.newVec(self.x, self.y), self.target)]
                GameData.bullets = GameData.bullets + temporary_bullet
                GameData.actors = GameData.actors + temporary_bullet
                soundbank.shoot.play
                break
            end if
        end for
    else if time-self.activeSince > self.timeOnTarget then
        self.active = false
    end if
end function

newTurret = function(position)
    retTurret = new Turret

    retTurret.x = position.x
    retTurret.y = position.y

    return retTurret
end function