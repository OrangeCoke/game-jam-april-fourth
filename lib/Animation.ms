
frames = []
currentFrame = 0
looping = true
framesPerSecond = 1
playing = false
lastFrameTime = 0

addImage = function(path)
    self.frames = self.frames + [file.loadImage(path)]
end function

addImageDirectory = function(path)
    if file.info(path)["isDirectory"] then
        imageFiles = file.children(path)
        for image in imageFiles
            self.addImage(path + "/" + image)
        end for
    else
        print("Error: " + path + " is not a directory")
    end if
end function

update = function()
    if self.playing and (time - self.lastFrameTime)*self.framesPerSecond >= 1.0 then
        self.currentFrame = (self.currentFrame+1)%self.frames.len
        self.lastFrameTime = time
    end if
end function

play = function()
    if not self.playing then
        self.lastFrameTime = time
        self.playing = true
    end if
end function

frame = function()
    return self.frames[self.currentFrame]
end function

stop = function()
    self.playing = false
    self.currentFrame = 0
end function