

playerMaxHealth=-1
playerCurrentHealth=-1
playerSpeed=-1
playerJump=-1
ceilingClimb=-1
wallGrab=-1
wallClimb=-1
wallJump=-1
turretDmg=-1
bulletSpeed=-1


getMergedUpgrades = function(priorityUpgrades)
    upgrades = new self
    if priorityUpgrades.playerMaxHealth>=0     then upgrades.playerMaxHealth     = priorityUpgrades.playerMaxHealth
    if priorityUpgrades.playerCurrentHealth>=0 then upgrades.playerCurrentHealth = priorityUpgrades.playerCurrentHealth
    if priorityUpgrades.playerSpeed>=0         then upgrades.playerSpeed         = priorityUpgrades.playerSpeed
    if priorityUpgrades.playerJump>=0          then upgrades.playerJump          = priorityUpgrades.playerJump

    if priorityUpgrades.ceilingClimb>=0 then upgrades.ceilingClimb = priorityUpgrades.ceilingClimb
    if priorityUpgrades.wallGrab>=0     then upgrades.wallGrab     = priorityUpgrades.wallGrab
    if priorityUpgrades.wallJump>=0     then upgrades.wallJump     = priorityUpgrades.wallJump
    if priorityUpgrades.wallClimb>=0    then upgrades.wallClimb    = priorityUpgrades.wallClimb

    if priorityUpgrades.turretDmg>=0    then upgrades.turretDmg    = priorityUpgrades.turretDmg
    if priorityUpgrades.bulletSpeed>=0  then upgrades.bulletSpeed  = priorityUpgrades.bulletSpeed
    //Add all new Upgrades above here
    return upgrades
end function

setToDefaultUpgrades = function ()
    self.playerMaxHealth=2
    self.playerCurrentHealth=self.playerMaxHealth
    self.playerSpeed=80
    self.playerJump=6
    self.wallGrab=1
    self.turretDmg=1
    self.bulletSpeed=1
end function