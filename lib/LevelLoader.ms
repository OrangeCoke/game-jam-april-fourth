import "json"
//Functions for loading levels

importLevel = function(path)
    levelFile = file.open(path)
    levelFile = levelFile.read
    levelFile = json.parse(levelFile)
    retdictionary = {}
    retdictionary["tiles"] = levelFile["layers"][0]["data2D"]
    retdictionary["turrets"] = levelFile["layers"][1]["entities"]
    retdictionary["start"] = levelFile["layers"][2]["entities"]
    retdictionary["goal"] = levelFile["layers"][3]["entities"]
    return retdictionary
end function

loadNextLevel = function (levelList,nextLevel) 
    //print "Loading level"
    GameData.actors = []
    GameData.detectableNoises = []
    GameData.bullets = []
    if nextLevel>0 or nextLevel==-1 then
        if nextLevel==-1 then nextLevel=0
        level=levelList.levels[nextLevel]
        //print "Loading level: "+nextLevel
        level.initFromDictionary(LevelLoader.importLevel(level.tileFile))
        //print "Level: "+level.levelNumber
        return level
    else
        print "End of Levels"
        return 0
    end if
end function