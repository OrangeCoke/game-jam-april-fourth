

max = function(seq) 
    if seq.len == 0 then return null 
    max = seq[0] 
    for item in seq 
        if item > max then max = item 
    end for 
    return max 
end function 

min = function(seq) 
    if seq.len == 0 then return null 
    min = seq[0] 
    for item in seq 
        if item < min then min = item 
    end for 
    return min
end function 

almostZero = function(value, epsilon)
    return value <= 0 and value + epsilon >= 0 or value >= 0 and value - epsilon <= 0
end function

Vector = {}
Vector.x=0
Vector.y=0

Vector.newVec = function(x,y)
    newVec = new Vector
    newVec.x = x
    newVec.y = y
    return newVec
end function
Vector.scalarProduct = function(scalar)
    newVec = new Vector
    newVec.x = self.x*scalar
    newVec.y = self.y*scalar
    return newVec
end function

Vector.sum = function(vec1, vec2)
    newVec = new Vector
    newVec.x = vec1.x+vec2.x
    newVec.y = vec1.y+vec2.y
    return newVec    
end function

Vector.length = function()
    return sqrt(self.x*self.x+self.y*self.y)
end function

Vector.normalize = function()
    return self.newVec(self.x/self.length, self.y/self.length)
end function

Vector.distance = function(vector)
    return sqrt((vector.x-self.x)*(vector.x-self.x)+(vector.y-self.y)*(vector.y-self.y))
end function