//CONSTANTS
GRAVITATION = -18
TERMINAL_VELOCITY = -10
FRICTION = 0.8

//IMPORTS

import "GameMath"
import "GameData"

checkCollison = function (x, y)
    return display(5).cell(x, y)
end function

getTilebyPixel = function (x, y)
    return GameMath.Vector.newVec(floor(x/TileCellSize), floor(y/TileCellSize))
end function

getOverlappingTiles = function (actor,movement)
    newPos = GameMath.Vector.sum(actor.worldBounds,movement)

    tile=getTilebyPixel(newPos.x-(actor.worldBounds.width/2), 
                        newPos.y-(actor.worldBounds.height/2))//Check unten links
    minMaxXY={}
    minMaxXY.minY=tile.y
    minMaxXY.minX=tile.x
    tile=getTilebyPixel(newPos.x+(actor.worldBounds.width/2), 
                        newPos.y+(actor.worldBounds.height/2))//Check oben rechts 
    minMaxXY.maxY=tile.y
    minMaxXY.maxX=tile.x

    return minMaxXY
end function

isCollidingWithTile = function (actor,movement)
    minMaxXY=getOverlappingTiles(actor,movement)    
    for i in range (minMaxXY.minX,minMaxXY.maxX)
        for j in range (minMaxXY.minY,minMaxXY.maxY)
            if checkCollison(i,j) then return true
        end for      
    end for 
    return false            
end function

tileCollisionCalculation = function (actor)
    actor.resetCollision()
    if isCollidingWithTile(actor,actor.movement) then
        // if isCollidingWithTile(actor,actor.movement) and GameMath.almostZero(actor.movement.x,0.5) and GameMath.almostZero(actor.movement.y,0.5) then
        //     print "stuck"
        //     wait 3
        // end if
        collidingUpDown    = not isCollidingWithTile(actor,GameMath.Vector.newVec(actor.movement.x,0))
        collidingLeftRight = not isCollidingWithTile(actor,GameMath.Vector.newVec(0,actor.movement.y))
        //print collidingUpDown+" "+collidingLeftRight
        if (not collidingUpDown) and (not collidingLeftRight) then
            collidingUpDown = true
            collidingLeftRight = true
        end if

        if collidingUpDown then 
            if actor.movement.y<=0 then
                actor.isOnFloor=true
            else 
                actor.isOnCeiling=true
            end if
        end if

        if collidingLeftRight then actor.isOnWall=true
        
        counter=0
        while (isCollidingWithTile(actor,actor.movement)!=0)
            if counter>20 then 
                //Stuck in Tile, revert last Movement
                //print "Stuck in Tile, revert last Movement"
                actor.x=actor.x-actor.lastMovement.x
                actor.y=actor.y-actor.lastMovement.y
            end if
            counter=counter+1
            factX=1
            factY=1
            if collidingLeftRight then 
                if globals.lowFps then
                    factX=0.4
                else
                    factX=0.8
                end if
                
                if actor.movement.y<0 then factY=GameData.getCurrentPlayerUpgrades.wallGrab //Sticking to Wall adjustable
            end if
            if collidingUpDown then
                if actor.isOnFloor and GameMath.almostZero(actor.movement.y,2) then actor.movement.y=0
                if globals.lowFps then
                    factY=0.4
                else
                    factY=0.8
                end if
            end if

            
            // print "x:"+actor.x+" y:"+actor.y
            // print "mov x:"+actor.movement.x+" mov y:"+actor.movement.y+ char(13)
            // wait 0.5
            if globals.lowFps then
                if GameMath.almostZero(actor.movement.x,1.5) then actor.movement.x=0
                if GameMath.almostZero(actor.movement.y,0.3) then actor.movement.y=0
            else
                if GameMath.almostZero(actor.movement.x,0.5) then actor.movement.x=0
                if GameMath.almostZero(actor.movement.y,0.1) then actor.movement.y=0
            end if
            //wait 0.01
            actor.movement=GameMath.Vector.newVec(actor.movement.x*factX,actor.movement.y*factY)
        end while
        //if counter>1 then print counter
    end if
    return actor
end function


gravitation = function(movement, delta)
    changed_movement = new GameMath.Vector
    changed_movement.x = movement.x
    if globals.lowFps then
        changed_movement.y = GameMath.max([movement.y + GRAVITATION*2*delta, TERMINAL_VELOCITY])
    else
        changed_movement.y = GameMath.max([movement.y + GRAVITATION*delta, TERMINAL_VELOCITY])
    end if
    
    return changed_movement
end function

friction = function(movement)
    changed_movement = new GameMath.Vector
    if globals.lowFps then
        changed_movement.x = movement.x*FRICTION
    else
        changed_movement.x = movement.x*FRICTION
    end if

    if globals.lowFps then
        if GameMath.almostZero(changed_movement.x,2) then
            changed_movement.x = 0
        end if
    else
        if GameMath.almostZero(changed_movement.x,0.5) then
            changed_movement.x = 0
        end if
    end if

    changed_movement.y = movement.y
    return changed_movement
end function

get_movement_vector = function(direction, movement_vector, delta)
    if globals.lowFps then
        movement_vector.x = movement_vector.x+direction.x*delta*GameData.getCurrentPlayerUpgrades.playerSpeed*1.3
    else
        movement_vector.x = movement_vector.x+direction.x*delta*GameData.getCurrentPlayerUpgrades.playerSpeed
    end if
    if globals.lowFps then
        if direction.y then movement_vector.y = GameData.getCurrentPlayerUpgrades.playerJump*direction.y*2.8//Set to JumpSpeed, no delta
    else
        if direction.y then movement_vector.y = GameData.getCurrentPlayerUpgrades.playerJump*direction.y//Set to JumpSpeed, no delta
    end if
    
    return movement_vector
end function