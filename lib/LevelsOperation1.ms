import "Level"

levels=[]

//Next Level
level = new Level 
level.init

level.tileFile = "/usr/game/data/level_1.json"
level.levelNumber=-1
level.nextLevel=levels.len+1

levels= levels + [level]


//Next Level
level = new Level 
level.init

level.tileFile = "/usr/game/data/level_2.json"
level.levelNumber=levels.len
level.nextLevel=levels.len+1

levels= levels + [level]


//Next Level
level = new Level 
level.init

level.tileFile = "/usr/game/data/level_3.json"
level.levelNumber=levels.len
level.nextLevel=levels.len+1

levels= levels + [level]


//Next Level
level = new Level 
level.init

level.tileFile = "/usr/game/data/level_4.json"
level.levelNumber=levels.len
level.nextLevel=levels.len+1
level.levelUpgrades.wallGrab=0.94

levels= levels + [level]


//Next Level
level = new Level 
level.init

level.tileFile = "/usr/game/data/level_5.json"
level.levelNumber=levels.len
level.nextLevel=levels.len+1
level.levelUpgrades.wallGrab=0.94

levels= levels + [level]


//Next Level
level = new Level 
level.init

level.tileFile = "/usr/game/data/level_6.json"
level.levelNumber=levels.len
level.nextLevel=levels.len+1
level.levelUpgrades.wallGrab=0.94

levels= levels + [level]


//Next Level
level = new Level 
level.init

level.tileFile = "/usr/game/data/level_7.json"
level.levelNumber=levels.len
level.nextLevel=-1
level.levelUpgrades.wallGrab=0.94

levels= levels + [level]