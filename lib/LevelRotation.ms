import "Level"



getNextLevel = function(difficulty)
    levelIndex = ((difficulty-1)%(LevelsOperation1.levels.len-4))+3
    //print "in Rotation diff="+difficulty+" levelIndex="+levelIndex+ "turretDmg: "+(floor((difficulty-7)/((LevelsOperation1.levels.len-3)*2))+2)
    //wait 1
    level = LevelLoader.loadNextLevel(LevelsOperation1,levelIndex)
    level.tempUpgrades.turretDmg=floor((difficulty-7)/((LevelsOperation1.levels.len-3)*2))+2

    //7 DMG=2
    if difficulty>=7 then level.tempUpgrades.playerSpeed=100
    if difficulty>=8 then level.tempUpgrades.playerMaxHealth=5
    if difficulty>=8 then level.tempUpgrades.playerCurrentHealth=5
    if difficulty>=9 then level.tempUpgrades.bulletSpeed=1.3

    if difficulty>=11 then level.tempUpgrades.wallGrab=0.90
    if difficulty>=11 then level.tempUpgrades.wallClimb=1

    //15 DMG=3
    if difficulty>=15 then level.tempUpgrades.wallJump=1
    if difficulty>=15 then level.tempUpgrades.wallClimb=0
    if difficulty>=18 then level.tempUpgrades.playerSpeed=120
    if difficulty>=19 then level.tempUpgrades.bulletSpeed=1.5
    if difficulty>=21 then level.tempUpgrades.ceilingClimb=1
    //23 DMG=4
    if difficulty>=25 then level.tempUpgrades.playerSpeed=140
    if difficulty>=29 then level.tempUpgrades.playerMaxHealth=8
    if difficulty>=29 then level.tempUpgrades.playerCurrentHealth=8
    return level
end function


