
currentView = {}

//LevelView
LevelView = {}
LevelView.update = function(delta)

    GameData.player.movement = GamePhysics.get_movement_vector(GameData.player.controller.get_movement_dir(), GameData.player.movement, delta)
    GameData.player.movement = GamePhysics.gravitation(GameData.player.movement, delta)
    GameData.player.movement = GamePhysics.friction(GameData.player.movement)
    GameData.player = GamePhysics.tileCollisionCalculation(GameData.player)
    move_actor(GameData.player, GameData.player.movement)

    for bullet in GameData.bullets
        move_actor(bullet, bullet.movement)
    end for

    for actor in GameData.actors
        actor.update
    end for
    
    GameData.player.updateAnimationAccordingToMovementVector()//muss nach update sein


    display(4).sprites = GameData.actors
    updateNoises()

    GameHud.updateHud()
    
    GameData.level.updateLevelConditions()

    if key.pressed("l") then //Cheat
        print "Cheat won"
        GameData.level.win=true
    end if
    if GameData.level.win then 
        //print "Level Complete"
        GameData.level.win=false
        GameMenuView.nextLevel=GameData.level.nextLevel
        GameMenuView.show()
    end if
    if GameData.level.lost then 
        //print "Level Failed Reload Level: "+GameData.level.levelNumber
        GameData.level.lost=false
        outer.GameMenuView.nextLevel=GameData.level.levelNumber
        GameMenuView.show()
    end if
    if key.pressed("escape") then
        outer.GameMenuView.nextLevel=GameData.level.levelNumber
        GameMenuView.show()
        wait 0.2
    end if
end function

LevelView.show = function()
    GameData.lastUpdate=time
    outer.currentView=LevelView
    clear
    display(5).mode = displayMode.tile
    display(5).clear
    display(2).mode = displayMode.pixel
    display(2).clear
    display(1).mode = displayMode.pixel
    display(1).clear
    display(0).mode = displayMode.pixel
    display(0).clear
    if not GameData.inLevelRotation then
        GameData.level = LevelLoader.loadNextLevel(LevelsOperation1,self.nextLevel)
    else
        GameData.level = LevelRotation.getNextLevel(GameData.difficulty-1)
    end if

end function

LevelView.setLevel = function(levelNumber)
    if levelNumber then
        self.nextLevel=levelNumber
    else   
        self.nextLevel=-1
    end if
end function

//GameMenuView
GameMenuView = {}
GameMenuView.update = function(delta)
    if key.pressed("space") then
        outer.LevelView.setLevel(self.nextLevel)
        outer.LevelView.show()
    end if


end function

GameMenuView.show = function()
    clear
    display(5).mode = displayMode.tile
    display(5).clear
    display(2).mode = displayMode.pixel
    display(2).clear
    display(1).mode = displayMode.pixel
    display(1).clear
    display(0).mode = displayMode.pixel
    display(0).clear
    GameData.lastUpdate=time
    outer.currentView=GameMenuView

    if GameData.level.loaded then

        if self.nextLevel==GameData.level.levelNumber then
            print "Level Failed"
            print "Press Space to Restart Level"
        else if self.nextLevel==GameData.level.nextLevel then
            print "Level "+GameData.difficulty+" Complete"
            print "Press Space to start Next Level"
            if (GameData.level.nextLevel==-1) then 
                GameData.inLevelRotation=true
            end if
            if GameData.difficulty==3 then 
                print char(13)+"New Abillity: Wall Grabbing"
                print char(13)+"Slowly slide down walls"
            end if
            if GameData.difficulty==7  then print char(13)+"+ Running Speed Increased"
            if GameData.difficulty==7  then print char(13)+"- Dmg increased"
            if GameData.difficulty==8  then print char(13)+"+ More Health"
            if GameData.difficulty==9  then print char(13)+"- Bullet Speed increased"
            if GameData.difficulty==11 then print char(13)+"New Abillity: Wall Climbing"

            if GameData.difficulty==15 then print char(13)+"New Abillity: Wall Jumping"
            if GameData.difficulty==15 then print char(13)+"- Dmg increased"
            if GameData.difficulty==18 then print char(13)+"+ Running Speed Increased"
            if GameData.difficulty==19 then print char(13)+"- Bullet Speed increased"
            if GameData.difficulty==21 then print char(13)+"New Abillity: Ceiling Climbing"
            if GameData.difficulty==23 then print char(13)+"- Dmg increased"
            if GameData.difficulty==29 then print char(13)+"+ More Health"
            

            GameData.difficulty=GameData.difficulty+1
        end if
    else 
        print "Welcome to Mini Micro Ninja"
        print "Press Space to Start"
    end if
end function


//MainMenuView
MainMenuView = {}
MainMenuView.update = function(delta)

end function

MainMenuView.show = function()
    clear
end function