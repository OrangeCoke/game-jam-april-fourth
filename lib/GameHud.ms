
displayId=0
updateHud= function()
    hud=display(outer.displayId)
    display(outer.displayId).mode = displayMode.off
    updateHealth(hud)
    display(outer.displayId).mode = displayMode.pixel
    if outer.displayId then 
        outer.displayId=0
    else 
        outer.displayId=1
    end if
    display(outer.displayId).clear
    //display(1).drawRect(self.position.x - self.currentRadius, self.position.y - self.currentRadius, 2*self.currentRadius, 2*self.currentRadius, "#FFFFFFFF")
end function
healthPosition = GameMath.Vector.newVec(792,590)
healthSize = GameMath.Vector.newVec(134,24)
updateHealth = function(hud)
    
    //Border
    hud.drawRect(healthPosition.x, healthPosition.y, healthSize.x, healthSize.y, "#FFFFFFFF")
    hud.drawRect(healthPosition.x+1, healthPosition.y+1, healthSize.x-2, healthSize.y-2, "#FFFFFFFF")
    //Health
    //display(1).fillRect(healthPosition.x+3, healthPosition.y+3, ((healthSize.x-6)/GameData.getCurrentPlayerUpgrades.playerMaxHealth)*GameData.getCurrentPlayerUpgrades.playerCurrentHealth, healthSize.y-6, "#FFFFFFFF")
    oneLifeWidth=floor((healthSize.x-6)/GameData.getCurrentPlayerUpgrades.playerMaxHealth)
    //print "Number of Lifes: "+(healthSize.x-6)
    if GameData.getCurrentPlayerUpgrades.playerCurrentHealth then
        for i in range (0,GameData.getCurrentPlayerUpgrades.playerCurrentHealth-1)
            hud.fillRect(healthPosition.x+3+(i*oneLifeWidth)+1, healthPosition.y+3, oneLifeWidth-2, healthSize.y-6, "#FFFFFFFF")
        end for
    end if
end function